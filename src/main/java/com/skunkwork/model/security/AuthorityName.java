package com.skunkwork.model.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
