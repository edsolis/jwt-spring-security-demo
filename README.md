# JWT Spring Security Demo

## Run
mvn spring-boot:run

## Links
http://localhost:8080/


## Test

### users
```
Admin - admin:admin
User - user:password
Disabled - disabled:password (this user is disabled)
```

curl -i -X POST http://localhost:8080/auth \
-H "Content-Type: application/json" \
-d '{"username":"admin", "password":"admin"}'


curl -i -X GET http://localhost:8080/persons \
-H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTU1MDY4MTQ0MSwiaWF0IjoxNTUwMDc2NjQxfQ.sYJDeqbpA3kczJiMKJVXWommymQamR6RylVjyCs0RF1jthAj_kHwhShH8bI4eJMas4HjVkpfXSOnFr98ERybqw"
-H "Content-Type: application/json" | python -m json.tool


curl -i -X POST http://localhost:8080/auth \
-H "Content-Type: application/json" \
-d '{"username":"user", "password":"password"}'

curl -i -X GET http://localhost:8080/protected \
-H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTU1MDY4MTQ0MSwiaWF0IjoxNTUwMDc2NjQxfQ.sYJDeqbpA3kczJiMKJVXWommymQamR6RylVjyCs0RF1jthAj_kHwhShH8bI4eJMas4HjVkpfXSOnFr98ERybqw"
-H "Content-Type: application/json"



